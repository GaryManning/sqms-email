<?php

/*
if ( $_SERVER['REMOTE_ADDR'] != "83.223.124.17" ) {
    // Does not come from 'www.raspberrysoftware.com'
    echo "Error: " . $_SERVER['REMOTE_ADDR'] . " - Invalid caller";
    exit;
}
*/

error_reporting(E_ALL);
ini_set('display_errors', '1');

header('Content-type: application/json');

//Check the parameters
if ( !isset($_REQUEST['db']) or !isset($_REQUEST['rid']) ) {
	echo "No paramaeters";
	if ( !isset($_SERVER['HTTP_HOST']) ) {
		parse_str($argv[0], $_REQUEST);
		parse_str($argv[1], $_REQUEST);

	} else {
		exit;
	}
}

/* Include the database credentials */
include 'db.inc';

$database = $_REQUEST['db'];

$field_prefix = "Insp";

$results_header = "${field_prefix}_ResultsHeader";
$result_details = "${field_prefix}_ResultDetails";

/*
if ( isset($_REQUEST['fault_report']) ) {
	$field_prefix = "Fault";
	$results_header = "${field_prefix}_ResultsHeader";
	$result_details = "${field_prefix}_ResultDetails";
}
*/

$connectionOptions = array("Database"=>"$database", "UID"=>"$user", "PWD"=>"$password");

$conn = sqlsrv_connect($server, $connectionOptions);

if ( $conn === false ) {
	die(FormatErrors(sqlsrv_errors()));
}

$messages = array();
$showMessages = true;

$data = array();

if ( isset($_REQUEST['test']) ) {
	echo "[{'test': 'Testing...'}]";
	exit;
}

if ( isset($_REQUEST['email']) ) {
	// Get the most appropriate email address for this question
	$data['email'] = getEmailAddress($_REQUEST['regid'], $_REQUEST['sid'], $_REQUEST['qid']);
	echo json_encode($data);
	exit;
}

$data['failures'] = getSurveyFailures($_REQUEST['rid']);

if ( sizeof($data['failures']) > 0 ) {
	$data['photos'] = getSurveyFailurePhotos($_REQUEST['rid']);
	$data['details'] = getSurveyDetails($_REQUEST['rid']);
	$rid = $data['details'][0]['REGID'];
	$sid = $data['details'][0]['SID'];
} else {
	// No failures so just exit - no need to send anything back
	exit;
}

echo json_encode($data);

/* Free statement and connection resources. */
sqlsrv_close($conn);

if ( count($messages) > 0 ) {
	echo json_encode($messages);
}

function ms_escape_string($data) {
		//http://stackoverflow.com/questions/574805/how-to-escape-strings-in-mssql-using-php
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
			$data = str_replace("'", "''", $data );
			return $data;
    }

function log_sql_metadata ($stmt) {
	foreach( sqlsrv_field_metadata($stmt) as $fieldMetadata ) {
		foreach( $fieldMetadata as $name => $value) {
			//$messages[] = array("debug" => "$name: $value");
			addMessage("debug", "$name: $value");
		}
	}
}

function log_error ($subject, $errors) {
	//$messages[] = array("error" => "Error while " . $subject . ":");
	addMessage("error", "Error while " . $subject . ":");
	foreach( $errors as $error ) {
		$message = preg_replace('/\[.*\]/', '', $error['message']);
		//$messages[] = array("error" => $message);
		addMessage("error", $message);
	}
	//$messages[] = array("error" => "");
	addMessage("error", "");
}

function log_error_to_temp ($error) {
	global $fh;

	$fh = fopen('php://temp','a+');
	if ( is_array($error) ) {
		foreach( $error as $err ) {
			fwrite($fh,$err['message']);
		}

	} else {
		fwrite($fh,$error);
	}

	fwrite($fh,"\r\n");
	fclose($fh);
}

function addMessage ($level, $message) {
	global $messages, $showMessages;

	if ( $level == "info" ) {
		if ( $showMessages ) {
			$messages[] = array($level => $message);
		}
	} else {
		$messages[] = array($level => $message);
	}
}

function getSurveyDetails ($rid) {
	global $field_prefix;

	// Get the details of the survey

	$query_string  = "select top 1 ${field_prefix}_RegionName as RegionName, ";
	$query_string .= "${field_prefix}_Regions.${field_prefix}_ShortName as ShortName, ";
	$query_string .= "${field_prefix}_Regions.${field_prefix}_REGID as REGID, ";
	$query_string .= "${field_prefix}_Stations.${field_prefix}_SID as SID, ";
	$query_string .= "${field_prefix}_StationName as StationName ";
	$query_string .= "from ${field_prefix}_ResultDetails ";
	$query_string .= "inner join ${field_prefix}_ResultsHeader on ${field_prefix}_ResultsHeader.${field_prefix}_RID = ${field_prefix}_ResultDetails.${field_prefix}_RID ";
	$query_string .= "inner join ${field_prefix}_Stations on ${field_prefix}_ResultsHeader.${field_prefix}_SID = ${field_prefix}_Stations.${field_prefix}_SID ";
	$query_string .= "inner join ${field_prefix}_Regions on ${field_prefix}_Regions.${field_prefix}_REGID = ${field_prefix}_Stations.${field_prefix}_REGID ";
	$query_string .= "where ${field_prefix}_ResultsHeader.${field_prefix}_RID = ? and ${field_prefix}_ResultDetails.${field_prefix}_MDID = 2";

	$failures = array();

	getData($query_string, $failures, array($rid));

	return $failures;

}

function getSurveyFailures ($rid) {
	global $field_prefix;

	// Get just the failed questions from the survey

	$query_string = " select ${field_prefix}_QuestionText as Question, ";
	$query_string .= "${field_prefix}_Notes as Notes, ";
	$query_string .= "${field_prefix}_ResultDetails.${field_prefix}_QID as QID ";
	$query_string .= "from ${field_prefix}_ResultDetails ";
	$query_string .= "inner join ${field_prefix}_ResultsHeader on ${field_prefix}_ResultsHeader.${field_prefix}_RID = ${field_prefix}_ResultDetails.${field_prefix}_RID ";
	$query_string .= "inner join ${field_prefix}_MasterQuestions on ${field_prefix}_MasterQuestions.${field_prefix}_QID = ${field_prefix}_ResultDetails.${field_prefix}_QID ";
	$query_string .= "where ${field_prefix}_ResultsHeader.${field_prefix}_RID = ? and ${field_prefix}_ResultDetails.${field_prefix}_MDID = 2";

	$failures = array();

	getData($query_string, $failures, array($rid));

	return $failures;

}

function getSurveyFailurePhotos ($rid) {
	global $field_prefix;

	// Get the failed questions that have a photo attached

	$query_string  = "select ${field_prefix}_ResultDetails.${field_prefix}_QID as QID ";
	$query_string .= "from ${field_prefix}_ResultDetails ";
	$query_string .= "inner join ${field_prefix}_ResultsHeader on ${field_prefix}_ResultsHeader.${field_prefix}_RID = ${field_prefix}_ResultDetails.${field_prefix}_RID ";
	$query_string .= "inner join ${field_prefix}_MasterQuestions on ${field_prefix}_MasterQuestions.${field_prefix}_QID = ${field_prefix}_ResultDetails.${field_prefix}_QID ";
	$query_string .= "where ${field_prefix}_ResultsHeader.${field_prefix}_RID = ? and ${field_prefix}_ResultDetails.${field_prefix}_MDID = 2 and ${field_prefix}_Photo is not null";

	$failures = array();
	$photos = array();

	getData($query_string, $failures, array($rid));

	foreach ($failures as $failure) {
		// Rebuild the array so that it is indexed by the QID
		$photos[$failure['QID']] = true;
	}

	return $photos;

}

function getEmailAddress ($regid, $sid, $qid) {
	global $field_prefix;

	// Get the email addresses for the failed questions
	// Finds an email address for the station/location/question if it exists
	// If it does not then find the email address for the station/location
	// Or for the Station.
	// If that all fails then just select the default email address (all nulls in the REGID, SID and QID columns)

	$query_string  = "select ${field_prefix}_EmailAddresses.${field_prefix}_email as email ";
	$query_string .= "from ${field_prefix}_EmailAddresses ";
	$query_string .= "where (${field_prefix}_REGID = ? or ${field_prefix}_REGID is NULL) ";
	$query_string .= "and (${field_prefix}_SID = ? or ${field_prefix}_SID is NULL) ";
	$query_string .= "and (${field_prefix}_QID = ? or ${field_prefix}_QID is NULL) ";

	$email = array();

	getData($query_string, $email, array($regid, $sid, $qid));

	// Just return the last item in the array (should be the appropriate email address)
	return array_pop($email);
}

function getData ($query, &$array, $params) {
	global $conn;

	$stmt = sqlsrv_query($conn, $query, $params);

	if( !$stmt ) {
		log_error("retrieving data", sqlsrv_errors());
		return;
	}

	while ( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) {

		$array[] = $row;

	}
}
?>
