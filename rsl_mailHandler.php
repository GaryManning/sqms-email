<?php

//ini_set('display_errors', 1);error_reporting(E_ALL);

if ( !isset($_REQUEST['db']) or !isset($_REQUEST['rid']) ) {
    echo "Missing parameters";
    exit;
}

$params = "db=" . $_REQUEST['db'] . "&rid=" . $_REQUEST['rid'];

$server      = "https://apps1.raspberrysoftware.com/Northern/SQMS/email";
$dbServer      = "$server/rsl_mailHandler-server.php?$params";
$imageServer = "$server/download_image.php?$params";

$img = '<img src="cid:image" alt="Failure image">';

/* Include the email address details */
include 'web.inc';

$context = stream_context_create(array(
        'http'=>array(
                'header'=>array('Authorization: Basic ' . base64_encode("$webUser:$webpw"))
                )
        ));

$failures = file_get_contents($dbServer, false, $context);

if ( strlen($failures) == 0 ) {
        // If there are no failures then just exit
        exit;
}

$data = json_decode($failures, true);

$station      = $data['details'][0]['RegionName'];
$regid        = $data['details'][0]['REGID'];
$shortStation = $data['details'][0]['ShortName'];
$location     = $data['details'][0]['StationName'];
$sid          = $data['details'][0]['SID'];

require 'PHPMailer/PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;

/* Commented code only needed for SMTP mailing on Windows
//Tell PHPMailer to use SMTP
$mail->isSMTP();

//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;

//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';         // Using gmail during testing

// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = $emailUser;

//Password to use for SMTP authentication
$mail->Password = $emailPass;

// End of commented Windows code */

//Set who the message is to be sent from
$mail->setFrom($emailFrom, $emailFromFull, FALSE);

//Set an alternative reply-to address
$mail->addReplyTo($emailReply, $emailReplyFull);

// For each failure, create an email, get the required receipient, add any photos, and send the email
foreach($data['failures'] as $failure) {

        $question = $failure['Question'];
        $qid = $failure['QID'];
        $notes = $failure['Notes'];
        if ( $notes == "" ) {
                $notes = "[None entered]";
        }

        // Get the email template
        $content = file_get_contents('content.html');

        // Replace the anchor with the appropriate text
        $content = str_replace('%station%', $station, $content);
        $content = str_replace('%shortStation%', $shortStation, $content);
        $content = str_replace('%location%', $location, $content);
        $content = str_replace('%question%', $question, $content);
        $content = str_replace('%notes%', $notes, $content);

        //Get the addressee from the database...
        $emails = json_decode(file_get_contents($dbServer . "&email=1&regid=" . $regid . "&sid=" . $sid . "&qid=" . $qid, false, $context), true);

        // Split the resulting address into separate addressees if required
        foreach(preg_split("/;/", $emails['email']['email']) as $email) {
                $mail->addAddress($email);
        }

        //Set the subject line
        //Make it something appropriate concerning the SQMS area that failed
        $mail->Subject = "Survey Failure Report - $station";

        //Embed a photo if one exists
        if ( isset($data['photos'][$qid]) ) {
                $mail->addStringEmbeddedImage(file_get_contents($imageServer . "&qid=" . $qid, false, $context), "image", 'image.jpg');
                $content = str_replace('%photo%', $img, $content);
        } else {
                $content = str_replace('%photo%', "", $content);
        }

        // Add the content to the message
        $mail->msgHTML($content, dirname(__FILE__));

        //send the message, check for errors
        if (!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
        }

        $mail->clearAddresses();
        $mail->clearAttachments();
}
