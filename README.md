SQMS Email scripts

Called by the middle-ware when a survey is submitted, to email the appropriate person(s) to fix any issues

rsl_emailHandler.php - parameters: $db - database, $rid - resultsHeader id
  (Requires PHPMailer)

https://apps1.raspberrysoftware.com/Northern/email/rsl_mailHandler.php?db=[database]&rid=[resultsHeaderId]

Amendments - 23Feb17
Due to the client not allowing us to use their SMTP server we had to re-jig things slightly so that we
can use the sendmail client on our external web-site (www.raspberrysoftware.com).

As a result the rsl_mailHandler.php script (along with the PHPMailer.php library, the content.txt file
and the web.inc file) resides on the external server in public_html/Northern/SQMS/email along with the
supporting files web.inc (email addresses etc), content.txt (email template) and PHPMailer.php (php
mailing library).

That means that although all the files are kept in a single location in git once deployed, the main script
and the support files need to be copied to the location on the external so that it can be called by the
middleware with:

http://www.raspberrysoftware.com/Northern/SQMS/email/rsl_mailHandler.php?db=[database]&rid=[resultsHeaderId]

The main script then makes calls back to the 'proper' server for its database access calls.


```

        apps1                                   cld-sql02
+------------------------------+            +--------------+
|                              |            |              |
| rsl_mailHandler-server.php <-+--------------> Database   |
|     ^                        |            |              |
|     |                        |            |              |
|     |                        |            |              |
+-----+------------------------+            +--------------+
      |
      |    www.raspberrysoftware.com
+-----+-----------------+
|     v                 |
| rsl_mailHandler.php   |
|     |                 |
|     v                 |
| sendmail              |
|     |                 |
+-----+-----------------+
      |
      v
    emails
```