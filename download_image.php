<?php 

if ( isset($_REQUEST['storage']) ) {
	$image_id = $_REQUEST['storage'];
	$query_string = "select image from Image_Store where IID = $image_id";
	}

$field_prefix = "Insp";
if ( isset($_REQUEST['fault_report']) ) {
	$field_prefix = "Fault";
}

$results_header = "${field_prefix}_ResultsHeader";
$result_details = "${field_prefix}_ResultDetails";

if ( isset($_REQUEST['qid']) ) {
	$image_id = $_REQUEST['qid'];
	$rid = $_REQUEST['rid'];
	$query_string = "select ${field_prefix}_Photo as image from $result_details where ${field_prefix}_RID = $rid and ${field_prefix}_QID = $image_id";
}

function log_error ($error) {
	$date = date("dMY H:i - ");
	$fh = fopen('C:\web\include\log.txt','a+');
	fwrite($fh,$date);
	fwrite($fh,$error);
	fwrite($fh,"\r\n");
	fclose($fh);
}

/* Include the database credentials */
include "db.inc";

if ( isset($_REQUEST['db']) ) {
	$database = $_REQUEST['db'];
}


$connOpts = array('Database'=>$database, 'UID'=>$user, 'PWD'=>$password, 'ReturnDatesAsStrings'=>'true');

$conn = sqlsrv_connect($server,$connOpts);

if (!$conn) {
	die("Database connection failure.");
}

$query = sqlsrv_query($conn, "$query_string");
if ( !$query ) {
	log_error("Error 702: Download of image failed. \$query_string: $query_string");
	exit;
}
$row = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC);

$image = $row['image'];

header("Content-Length: ".strlen($image));
header("Content-Type: image/jpeg");

echo $image;
?>
